import Api from "./Api";
import qs from "qs";

export default class PutawayApi extends Api {

    init = async () => {
        const url = this.getUrl("putaway/init");
        return await this.getClient().post(url);
    };

    searchProduct = async(barcode: number)  => {
        const url = this.getUrl(`putaway/search/products?barcode=${barcode}`);
        return await this.getClient().get(url);
    }

    searchStorage = async(value) => {
        const url = this.getUrl(`search/storages?term=${value}`);
        return await this.getClient().get(url);
    }

    assignStorage = async(params) => {
        const url = this.getUrl(`putaway/set-storage`);
        return await this.getClient().post(url, qs.stringify(params));
    }

    loadItems = async(id) => {
        const url = this.getUrl("putaway/"+id);
        return await this.getClient().get(url);
    }

    addItem = async(params) => {
        const url = this.getUrl(`putaway/item`);
        console.log(params);
        return await this.getClient().post(url, qs.stringify(params));
    }

    start = async(id: number) => {
        const url = this.getUrl("putaway/"+id+"/start");
        return await this.getClient().post(url);
    }

    close = async(id: number) => {
        const url = this.getUrl("putaway/"+id+"/close");
        return await this.getClient().post(url);
    }




}