import Axios, {AxiosInstance} from "axios";

interface Props {
  token?: string;
  warehouseId?: number;
}

export default class Api {
  private client: AxiosInstance;

  constructor({ token, warehouseId }: Props) {
    const headers = {
      Authorization: `Bearer ${token}`,
      WarehouseId: warehouseId,
      Version: process.env.NEXT_PUBLIC_APP_VERSION,
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    };
    this.client = Axios.create({ headers });
  }

  public getClient() {
    return this.client;
  }

  public getUrl(path: string) {
    return process.env.NEXT_PUBLIC_API_ENDPOINT + path;
  }
}
