import Api from "./Api";
import qs from "qs";
import { transferlist } from "./dummydata";


export default class TransferAPI extends Api {

    init = async () => {
        const url = this.getUrl("transfer/getlist");

        return transferlist;

       // return await this.getClient().post(url);  /migrations/{id}/products

    };
    //TODO change stubs 
    loadItems = async(id) => {
        const url = this.getUrl("migration/"+id);
        //const items =  transferlist.filter(item=> item.id == id);
        //return  items[0];
        return await this.getClient().get(url);
    }

    loadCart = async(pid) => {
        const url = this.getUrl("putaway/"+pid);
       // c//onst items =  transferlist.filter(item=> item.id == id);
        //return  items[0];
        return await this.getClient().get(url);
    }

    searchProduct = async(term: number, id: number, type?: string)  => {
        const url = this.getUrl(`migration/${id}/search/products?term=${term}&${type && "type="+type}`);
        return await this.getClient().get(url);
    }

    searchStorage = async(value) => {
        const url = this.getUrl(`search/storages?term=${value}`);
        return await this.getClient().get(url);
    }

    addItemToCart = async(params) => {
        const url = this.getUrl('putaway/carts/add-item');
        console.log('params ', params);
        return await this.getClient().post(url, qs.stringify(params));
    }


    getCart = async() => {
        const url = this.getUrl(`transfer/getcart`);
        
        return await this.getClient().get(url);
    }
   

    startMigration = async(id ) => {
        const url = this.getUrl('putaway/'+id+'/start');
        
        return await this.getClient().post(url);
    }

    closeMigration = async(id ) => {
        const url = this.getUrl('putaway/'+id+'/close');
        
        return await this.getClient().post(url);
    }

    migrateItem = async(id , params) => {
        //console.lg
        const url = this.getUrl("putaway/"+id+"/place-item");
        return await this.getClient().post(url,qs.stringify(params));
    }


}
