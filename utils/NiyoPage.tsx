import {RootState} from "../redux/store";
import {useSelector} from "react-redux";
import {useEffect} from "react";
import Router from 'next/router';

const mapRootState = (rootState: RootState) => {
    return {
        token: rootState.appState.token,
        isInitialized: rootState.appState.isInitialized
    };
}

const NiyoPage = ({ Component, ...pageProps }) => {
    const { token, isInitialized } = useSelector(mapRootState);
    const isPrivate = (Component as any).isPrivate;
    const blockAccess = (isPrivate === undefined || isPrivate === true) && !token;
    useEffect(() => {
        if (isInitialized && blockAccess) {
            Router.push('/login');
        }
    }, [blockAccess, isInitialized]);

    if (!isInitialized || blockAccess) {
        return null;
    }

    return <Component {...pageProps} />
}

export default NiyoPage;