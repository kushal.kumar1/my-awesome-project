export enum StorageKey {
    TOKEN = "token",
    USER = "user",
    WAREHOUSE = "warehouse",
    ACTIVE_WAREHOSUE = "active_warehouse",
}

const get = (key: StorageKey, defaultValue: any, throughSession: boolean = false) => {
    const valueJsonString = localStorage.getItem(key);
    return valueJsonString ? JSON.parse(valueJsonString) : defaultValue;
}

const set = (key: StorageKey, value: any, throughSession: boolean = false) => {
    localStorage.setItem(key, JSON.stringify(value));
}

const remove = (key: StorageKey) => {
    localStorage.removeItem(key);
}

const clear = () => {
    localStorage.clear();
}

export default { get, set, remove, clear }