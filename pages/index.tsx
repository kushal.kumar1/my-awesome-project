import { NextPage } from "next";
import Page from "../layouts/Page";
import DashCard from "../components/home/DashCard";
import { useDispatch } from "react-redux";

const IndexPage: NextPage = () => {
  const dispatch = useDispatch();
  return (
    <Page>
      <section>
        <div className="md bold pv-md p-sm">INBOUND</div>
        <div className="grid gutter-lg center">
          <DashCard faIcon="fas fa-pallet" color="orange" label="putaway" />
        </div>
      </section>

      <section>
        <div className="md bold pv-md p-sm mt-xl">INVENTORY</div>
        <div className="grid gutter-lg center">
          <DashCard faIcon="fas fa-boxes" color="blue" label="audit" />
          <DashCard
            faIcon="fas fa-exchange-alt"
            color="green"
            label="transfer"
          />
        </div>
      </section>
    </Page>
  );
};

export default IndexPage;
