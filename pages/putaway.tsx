import {NextPage} from "next";
import Page from "../layouts/Page";
import ListItem from "../components/putaway/ListItem";
import styled from "styled-components";
import FloatingButton from "../components/FloatingButton";
import Loader from "../components/Loader";
import EmptyMessage from "../components/EmptyMessage";
import useApi from "../hooks/useApi";
import Putaway from "../types/Putaway";
import {useDispatch} from "react-redux";
import {useRouter} from "next/router";
import PutawayAction from "../redux/actions/PutawayAction";

const Root = styled.div`
  .card:not(:first-child) {
    margin-top: var(--dimen-sm);
  }
`;

const PutawayPage: NextPage = () => {
    const apiConfig = {path: 'putaway'};
    const {results, loading} = useApi<Putaway[]>(apiConfig);
    const router = useRouter();

    const dispatch = useDispatch();
    const initPutaway = async () => {
        const id = await dispatch(PutawayAction.create());
        router.push('putaway/' + id);
    }

    return (
        <Page title="PUTAWAY LIST">
            <Root>
                {loading && <Loader/>}
                {results && results.length > 0 && results.map((item) => <ListItem item={item} key={item.id}/>)}
                {results && results.length == 0 &&
                <EmptyMessage message="Keep Rolling. Hit the [+] button!"></EmptyMessage>}
            </Root>
            <FloatingButton onClick={initPutaway}/>
        </Page>
    );
}

export default PutawayPage;
