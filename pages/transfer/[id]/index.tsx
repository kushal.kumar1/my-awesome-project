import Page from "../../../layouts/Page";
import React, {useEffect, useState} from "react";
import PutawayAction from "../../../redux/actions/PutawayAction";
import TransferAction from "../../../redux/actions/TransferAction";
import {GetServerSideProps} from "next";
import AppAction from "../../../redux/actions/AppAction";
import {ModalName} from "../../../components/ModalManager";
import TransferItem from '../../../components/transfer/TransferItem'
import {PutawayStatus} from "../../../types/Putaway";
import { TransferItem as TransferItemType, TransferItemStatus} from "../../../types/TransferItem";
import {useThunkyDispatch} from "../../../redux/store";
import EmptyMessage from "../../../components/EmptyMessage";
import Loader from "../../../components/Loader";
import FloatingButton from "../../../components/FloatingButton";
import { TransferStatus } from "../../../types/transfer";
import Link from 'next/link';
import styled from "styled-components"
import useApi from "../../../hooks/useApi";
import { AppActionTypes } from "../../../redux/ActionTypes";


const TransferDetailPage = ({id}) => {
    const [loading, setLoading] = useState(false);
    const [items, setItems] = useState<TransferItemType[] | []>([]);
    const [status, setStatus] = useState<string>(null);
    const [putawayid,setPutawayId] = useState<string>(null);
    const [cartItems,setCartItems] = useState<TransferItemType[] | []>([]); 
    const [remarks, setRemarks] = useState([])
    const dispatch = useThunkyDispatch();

    const load = async (id: number) => {
        setLoading(true);
        const data = await dispatch(TransferAction.load(id))
        console.log(data);
        if (data != null){  
        setItems(data.items);
        setStatus(data.status);
        setRemarks(data.from_remarks)
        setCartItems(data.items.filter(item => item.cart_quantity > 0 && item.status != "completed"));
        console.log("filtered item list",data.items.filter(item => item.status));
        console.log("%%", data.putaway_id)
        setPutawayId(data.putaway_id);
        }
        setLoading(false);
    };

    useEffect(() => {
        load(id);
    }, []);


    const canAddItemToCart = () => {
        return status == TransferStatus.OPEN && putawayid;
    }


    const addToCart = async (apiParams) => {
        setLoading(true)
        const addedtoCart = await dispatch(TransferAction.addItem(id, apiParams.product, apiParams.quantity, apiParams.source_location, apiParams.remarks, apiParams.migration_item_id));
        if(addedtoCart) {
            console.log('put away id', addedtoCart.id);
            setPutawayId(addedtoCart.id);
            const itemIndex = items.findIndex((item: TransferItemType) => item.product.id === apiParams.product);
            const item= items.find((item: TransferItemType) => item.product.id === apiParams.product);
            const updatedItem = {...item, cart_quantity: apiParams.quantity};
            items[itemIndex] = updatedItem;
            setCartItems([...cartItems, updatedItem]);
            setItems([...items]);
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: 'Successfully added item to cart', isError: false, milliseconds: 1000}
            });
        }
        setLoading(false)
        
    } 

    return (
        <Page title={`#${id}`}>
            <Root>
            <div className="grid gutter-sm c-center mt-md">
                <div className="col-6">
                    <div className="md bold mt-sm">ITEMS ({items.length})</div>
                </div>
                <div className="col-6 right">
                    <span className={`tag-${status} tag upper`}>{status}</span>
                </div>
            </div>

            <div className="grid gutter-sm mt-sm">
                {!loading && items && items.length > 0 && items.map(item =>
                    <TransferItem 
                        addToCart={addToCart} 
                        item={item} 
                        key={item.id} 
                        showStorageBtn={status == TransferStatus.OPEN}
                        migrationId ={id}
                        remarks={remarks}
                    />
                )}
                {!loading && items && items.length == 0 && <EmptyMessage message="No items found!"/>}
            </div>
          
            {loading && <Loader/>}
            {
                canAddItemToCart() && 
                <Link href={`/transfer/${id}/cart?pid=${putawayid}`} >
                <FloatingButton 
                    count={cartItems && cartItems.length}  //getting items from API
                    label="CART"
                    faClass="fas fa-shopping-cart"
                    onClick={()=>{}}
                />
                </Link>
            }
            </Root>
        </Page>

    );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
    const id = context.query.id;
    return {
        props: {
            id
        },
    }
}

const Root = styled.div`
  padding-bottom: 100px;
`;
export default TransferDetailPage;
