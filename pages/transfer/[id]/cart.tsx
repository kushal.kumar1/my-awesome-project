const { loadavg } = require("os")
const { useEffect, useState } = require("react")
import { useRouter } from "next/router";
import TransferAPI from "../../../apis/TransferAPI";
import EmptyMessage from "../../../components/EmptyMessage";
import Loader from "../../../components/Loader";
import CartItem from "../../../components/transfer/CartItem";
import Page from "../../../layouts/Page";
import { TransferItem as TransferItemType, TransferItemStatus} from "../../../types/TransferItem";
import TransferAction from "../../../redux/actions/TransferAction";
import Toolkit from "../../../utils/Toolkit";
import useApi from "../../../hooks/useApi";
import  Putaway, { PutawayStatus }  from "../../../types/Putaway";
import { GetServerSideProps } from "next";
import { useThunkyDispatch } from "../../../redux/store";
import { AppActionTypes } from "../../../redux/ActionTypes";

const CartPage = ({id}) => {
    const router = useRouter();
    const { pid } = router.query;

    console.log(router.query);
    ///console.log('id' , id );
    //console.log('pid --- ', pid)
    const apiConfig = {path: 'putaway/'+pid};
    const [cartItems, setCartItems] = useState([]);
    const [putawayStatus, setPutawayStatus] = useState('open');
    const [migrationItems,setMigrationItems] =useState([]);
    const [loading, setLoading] = useState(true);
    const [results,setResults] = useState();
    const dispatch  = useThunkyDispatch();
    
    const load = async (pid: any) => {
        setLoading(true);
        const data = await dispatch(TransferAction.loadCart(pid))
        setResults(data);
        setMigrationItems(data.items);
        setPutawayStatus(data.status);
       
        setLoading(false)
    };

    useEffect(() => {
          console.log("%%**", pid)
          load(pid);
    }, [])

    const startmigration = async () =>  {
       const startmigration = await dispatch(TransferAction.startMigration(pid));
       dispatch({
        type: AppActionTypes.DISPLAY_MESSAGE,
        payload: {text: 'migration started', isError: false, milliseconds: 2000}
    });
       setPutawayStatus('started');
    }

    const closemigration = async () =>  {
        try{
            const closemigration = await dispatch(TransferAction.closeMigration(pid));
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: 'migration completed', isError: false, milliseconds: 2000}
            });
            if (closemigration){
                router.back();
            }
            setPutawayStatus('closed');
        }catch (err){
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: err.response.data.message, isError: false, milliseconds: 2000}
            });
        }  
    }

    const putawayItem = async (apiParams) =>{
        setLoading(true);
        console.log('putaway Items ', apiParams)
        const putawayItem = await dispatch(TransferAction.putawayItem(pid,apiParams));
        console.log('putaway success', putawayItem);
        if (putawayItem) {  
         const itemIndex =  migrationItems.findIndex((item) => item.id === apiParams.putaway_item_id)

         console.log('item index ', itemIndex );
            if (apiParams.no_space ){
                // const  item =   putawayItem.
                    console.log('nospace');
                 migrationItems[itemIndex] = putawayItem.data;
                 migrationItems.push(putawayItem.excess_area_item );

                 //console.log(results);
                 setMigrationItems([...migrationItems]);

            } else {
                migrationItems[itemIndex] = putawayItem.data;
                setMigrationItems([...migrationItems]);
            }
         //putawayItem = putawayItem.

        }
        setLoading(false)
        console.log('putaway clicked');
    }
    return (
        <Page title={`CART`}>
            {!loading && migrationItems && migrationItems.length !== 0 ?
                <div className="grid gutter-sm">
                    <div className="col-6">
                    {putawayStatus == 'open' ?
                    <button className="btn btn-sm btn-orange w-full" onClick={startmigration} >
                            START MIGRATING
                        </button>
                        :
                        'In Progress'
                    }
                    </div>
                   
                    {putawayStatus == 'started' && <div className="col-6">
                        <button className="btn btn-sm btn-orange w-full" onClick={closemigration}>
                            FINISH
                        </button>
                    </div>
                    }
                    {migrationItems.map((item, key) => (
                        <CartItem 
                           migrationItems={migrationItems}
                            item={item} key={key}  
                        migrationstatus = {putawayStatus=='started'?true:false}
                        putawayItem = {putawayItem}
                        migrationId = {id}
                        />
                    ))}
                </div>
                :
                !loading && <EmptyMessage message="Cart is Empty" />
            }

            {
                loading && <Loader />
            }
        </Page>
    )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
    const id = context.query.id;
    const pid = context.query.pid?context.query.pid:null;

    return {
        props: {
            id,
            pid
        },
    }
}
export default CartPage