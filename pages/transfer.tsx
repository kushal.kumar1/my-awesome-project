import {NextPage} from "next";
import Page from "../layouts/Page";
import ListItem from "../components/transfer/TransferListItem";
import styled from "styled-components";
import FloatingButton from "../components/FloatingButton";
import Loader from "../components/Loader";
import EmptyMessage from "../components/EmptyMessage";
import useApi from "../hooks/useApi";
import {useDispatch} from "react-redux";
import {useRouter} from "next/router";
import PutawayAction from "../redux/actions/PutawayAction";
import Transfer from "../types/transfer";

const Root = styled.div`
  .card:not(:first-child) {
    margin-top: var(--dimen-sm);
  }
`;

const TransferPage: NextPage = () => {
    const apiConfig = {path: 'migration'};
    const {results, loading} = useApi<Transfer[]>(apiConfig);
    const router = useRouter();


    return (
        <Page title="TRANSFER LIST">
            <Root>
                {loading && <Loader/>}
                {results && results.length > 0 && results.map((item) => <ListItem item={item} key={item.id}/>)}
                {results && results.length == 0 &&
                <EmptyMessage message="Nothing assigned for migration yet."></EmptyMessage>}
            </Root>
        </Page>
    );
}

export default TransferPage;
