import Page from "../../layouts/Page";
import {useEffect, useState} from "react";
import PutawayAction from "../../redux/actions/PutawayAction";
import {GetServerSideProps} from "next";
import AppAction from "../../redux/actions/AppAction";
import {ModalName} from "../../components/ModalManager";
import {PutawayStatus} from "../../types/Putaway";
import PutawayItem from "../../components/putaway/PutawayItem";
import {PutawayItem as PutawayItemType} from "../../types/PutawayItem";
import {useThunkyDispatch} from "../../redux/store";
import Storage from "../../types/Storage";
import EmptyMessage from "../../components/EmptyMessage";
import Loader from "../../components/Loader";
import FloatingButton from "../../components/FloatingButton";
import useTimer from "../../hooks/useTimer";

const PutawayDetailPage = ({id}) => {
    const [loading, setLoading] = useState(false);
    const [items, setItems] = useState<PutawayItemType[] | []>([]);
    const [status, setStatus] = useState<string>(null);
    const dispatch = useThunkyDispatch();

    const load = async (id: number) => {
        setLoading(true);
        const data = await dispatch(PutawayAction.load(id))
        setItems(data.items);
        setStatus(data.status);
        setLoading(false);
    };

    const addItem = async (data) => {
        const newItem = await dispatch(PutawayAction.addItem(id, data.product, data.quantity));
        if(newItem) setItems([newItem, ...items])
    }

    const start = async () => {
        const data = await dispatch(PutawayAction.start(id));
        if(data) setStatus(data.status);
    }

    const close = async () => {
        const data = await dispatch(PutawayAction.close(id));
        if(data) setStatus(data.status);
    }

    const assignStorage = async (apiParams) => {
        const res = await dispatch(PutawayAction.assignStorage(apiParams));
        if(res) {
            const storage: Storage = res.storage;
            const item = items.find((item: PutawayItemType) => item.id === res.id);
            const updatedItem = {...item, storage};
            setItems([updatedItem, ...items.filter((item: PutawayItemType) => item.id != updatedItem.id)]);
        }
    }

    useEffect(() => {
        load(id);
    }, []);

    const canStart = () => {
        return status == PutawayStatus.OPEN && items.length > 0;
    }

    const canAddItem = () => {
        return status == PutawayStatus.OPEN;
    }

    const setBarcode = (barcode) => {
        setLoading(true);
        setTimeout(() => {
            setLoading(false);
            dispatch(AppAction.switchModal(ModalName.ADD_PRODUCT, {onSubmit: addItem, barcode: barcode}));
        },250);
    }

    const openScanner = () => {
        dispatch(AppAction.switchModal(ModalName.SCAN, {setBarcode: setBarcode}));
    }


    // @ts-ignore
    const emptyStorageItems = items.filter(item => item.storage === null);

    return (
        <Page title={`#${id}`}>
            {
                !loading && status != PutawayStatus.CLOSED &&
                <div className="grid gutter-sm">
                    <div className="col-6">
                        <button className="btn btn-sm btn-orange w-full" disabled={canAddItem() ? false : true}
                                onClick={() => dispatch(AppAction.switchModal(ModalName.ADD_PRODUCT, {onSubmit: addItem}))}>
                            <i className="fas fa-plus"></i> ADD ITEM
                        </button>
                    </div>
                    <div className="col-6">
                        {
                            status == PutawayStatus.STARTED &&
                            <button className="btn btn-sm btn-red w-full" disabled={emptyStorageItems.length == 0 ? false : true}
                                    onClick={close}><i className="fas fa-times"></i> CLOSE
                            </button>

                        }
                        {
                            emptyStorageItems.length >= 0 && status != PutawayStatus.STARTED  &&
                            <button className="btn btn-sm btn-yellow w-full" disabled={canStart() ? false : true}
                                    onClick={start}><i className="fas fa-play"></i> START
                            </button>

                        }
                    </div>
                </div>
            }
            <div className="grid gutter-sm c-center mt-md">
                <div className="col-6">
                    <div className="md bold mt-sm">ITEMS ({items.length})</div>
                </div>
                <div className="col-6 right">
                    <span className={`tag-${status} tag upper`}>{status}</span>
                </div>
            </div>

            <div className="grid gutter-sm mt-sm">
                {!loading && items && items.length > 0 && items.map(item =>
                    <PutawayItem item={item} key={item.id} showStorageBtn={status == PutawayStatus.STARTED}
                                 assignStorage={assignStorage}/>)
                }
                {!loading && items && items.length == 0 && <EmptyMessage message="No items found!"/>}
            </div>
            {loading && <Loader/>}
            {
                canAddItem() && <FloatingButton faClass="fas fa-barcode" onClick={openScanner}/>
            }

        </Page>

    );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
    const id = context.query.id;
    return {
        props: {
            id
        },
    }
}

export default PutawayDetailPage;
