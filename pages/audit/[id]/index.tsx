import Page from "../../../layouts/Page";
import React, {useEffect, useState} from "react";
import AuditAction from "../../../redux/actions/AuditAction";
import {GetServerSideProps} from "next";
import {useThunkyDispatch} from "../../../redux/store";
import EmptyMessage from "../../../components/EmptyMessage";
import Loader from "../../../components/Loader";
import { AuditStatus } from "../../../types/Audit";
import Link from 'next/link';
import styled from "styled-components"
import { AppActionTypes } from "../../../redux/ActionTypes";
import AuditGroupItem from "../../../components/audit/AuditGroupItem";

//import {audit} from '../../../apis/dummydata'


const AuditDetailsPage = ({id,audit_name}) => {
    const [loading, setLoading] = useState(false);
    const [items, setItems] = useState<any[] | []>([]); // to do type strict 
    //const [products, setProducts] = useState<any[] | []>([]); // to do type strict 
    const [auditType, setAuditType] = useState('')
    const [status, setStatus] = useState<string>(null);
    const dispatch = useThunkyDispatch();

    const load = async (id: number) => {
        setLoading(true);
        const data = await dispatch(AuditAction.load(id))
             console.log(data);  
        if (data!= null){ 
           // setItems(data.distinctProducts);
            setStatus(data.status);
            setAuditType(data.type);
            if (data.type ==='product'){
                setItems(data.distinctProducts);
            } else {
               // console
                setItems(data.distinctStorages);
            }
        }
       // setCartItems(data.items.filter(item => item.cart_item != null && item.status != "completed"));
       // setPutawayId(data.putaway_id);
        setLoading(false);
    };

    useEffect(() => {
        load(id);
    }, []);


    const addProdcutToStore = async (item) =>{
        dispatch({
            type: AppActionTypes.ADD_AUDIT_ITEM,
            payload: {currentAuditItem: item}
        });

    }

    return (
        <Page title={<p>
            #{id} {audit_name}
            <br/>
            {auditType == "product" ? "Product Wise" : "Location Wise"}
        </p>}>
            <Root>
            <div className="grid gutter-sm c-center mt-md">
                <div className="col-8">
                    <div className="md bold mt-sm">{auditType =='storage'?'LOCATIONS':'ITEMS'} ({items.length})</div>
                </div>
                

            </div>

            <div className="grid gutter-sm mt-sm">
                {!loading && items && items.length > 0 && items.map(item =>
                    <AuditGroupItem 
                        auditid={id}
                        item={item} 
                       // addProdcutToStore= {addProdcutToStore}
                        audittype ={auditType}
                        key={item.id} 
                        auditName={audit_name}
                    />
                )}
                {!loading && items && items.length == 0 && <EmptyMessage message="No items found!"/>}
            </div>
          
            {loading && <Loader/>}
           
            </Root>
        </Page>

    );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
    const id = context.query.id;
    const audit_name = context.query.audit_name
    return {
        props: {
            id,
            audit_name
        },
    }
}

const Root = styled.div`
  padding-bottom: 100px;
`;
export default AuditDetailsPage;
