import { GetServerSideProps, NextPage } from "next";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Page from "../../../../layouts/Page";
import styled from "styled-components";
import ProductDetailsCard from "../../../../components/audit/productPage/ProductDetailsCard";
import CustomPrimaryButton from "../../../../components/buttons/CustomPrimaryButton";
import AppActions from "../../../../redux/actions/AppAction";
import { ModalName } from "../../../../components/ModalManager";
import router from "next/router";
import { RootState, useThunkyDispatch } from "../../../../redux/store";
import AuditAction from "../../../../redux/actions/AuditAction";
import { AppActionTypes } from "../../../../redux/ActionTypes";
import { route } from "next/dist/server/router";

const Card = styled.div`
    box-shadow: 1px 1px 3px 0px #8f8f8f;
    border-radius: 4px;

    .card {
        min-height: 82px;
    }
`;

enum CardActiveSequence {
    QUANTITY = 1,
    BAD_STOCK = 2,
    INCORRECT_PRODUCT_INFO = 3,
}
const AuditProductDetail = ({ id,itemid , storageid }) => {
    console.log('id , auditid', id , itemid);
    const dispatch = useThunkyDispatch();

    const rootState = useSelector((state: RootState) => state);

    const [quantity, setQuantity] = useState(0);
    const [currentCardActive, setCurrentCardActive] = useState(1);
    const [badQuantity,setbadQuantity] = useState([]);
;   const [remarks, setRemarks] = useState("");
    const [loading,setLoading] = useState(false);
    const   [currentAuditItem,setCurrentAuditItem] = useState(rootState.appState.extraItem); 
    console.log(currentAuditItem);
    
   
    const load =  async () => {
        setLoading(true);
        const data =  await dispatch(AuditAction.getProduct(itemid));
             console.log('data ---', data);  
        if (data!= null && data.length !== 0){ 
           setCurrentAuditItem(data)
          //currentAuditItem = data;
            
            }
        
        setLoading(false);
    };

    
    useEffect(()=>{
        console.log('inside useeffect')
        if (currentAuditItem == null )
        load();
    },[])

    

    const handleQuantityChange = (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        const qt = event.target.value;
        setQuantity(parseInt(qt) === NaN ? 0 : parseInt(qt));
    };

    const handleQuantityButton = () => {
        handleQuantityDone();
    };

    const handleQuantityDone = () => {
        console.log("quantity done");
        setCurrentCardActive(2);
    };

    const handleReportBadStockButton = () => {
        dispatch(
            AppActions.switchModal(ModalName.BAD_STOCK, {
                action: handleBadStockDone,

                message: "Bad Stock",
            })
        );
    };

    const handleBadStockDone = (reasonList) => {
        console.log("Bad Stock Done");
        setbadQuantity(reasonList);
        console.log(' tesss', reasonList)
        setCurrentCardActive(3);
    };

    const handleRaiseIssueButton = () => {
        dispatch(
            AppActions.switchModal(ModalName.INCORRECT_PRODUCT_INFO, {
                action: handleIncorrectInfo,
                message: "Raise Issue For Wrong Info",
            })
        );
    };

    const handleIncorrectInfo = (remarks) => {
        setCurrentCardActive(4); //so that incorrect info button becomes disabled
        //setRemarks 
        setRemarks(remarks);
        console.log("incorrect info done");
    };


    const handleSubmitButton = async () => {
        let goodQuantiy = quantity;
         
         let badStock = [];
         if (badQuantity != null && badQuantity != []){
          badQuantity.forEach(qty=>{
            badStock.push({'condition':'bad',...qty})
            goodQuantiy = goodQuantiy - qty.quantity;

          })
        }

          let goodStock = {
            'condition' : 'good',
            'quantity' : goodQuantiy
        }
         let data ={
            productId: itemid,
            productBarcode : currentAuditItem.barcode,
            storageId:storageid,
            inputs :[goodStock,...badStock]

         }  
         console.log('input data', data);
         try  { 
        const result = await dispatch(AuditAction.addExtraInventory(id,data));

        console.log(' results ', result);

        //const submitResult =  await (dispatch(AuditAction.submitItem(id,itemid)));
        
        router.push(`/audit/${id}/items?location=${storageid}`);
            
            
        dispatch({
            type: AppActionTypes.DISPLAY_MESSAGE,
            payload: {text: "Data Submitted Successfully", isError: false, milliseconds: 2000}
        });

         } catch {

            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: "Error submitting data", isError: true, milliseconds: 2000}
            });

         }
        
         

    };

    return (
        <Page title="Product Details">
            <ProductDetailsCard product={currentAuditItem !==null &&  currentAuditItem} />
            <Card className="mt-md">
                <div className="col-12 card p-md grid">
                    <div className="col-9">
                        <h3 className="bold">
                            Quantity
                        </h3>
                        <span className="sm">Total no. of items audited</span>
                        <span className="sm">
                            Total no. of items{" "}
                        </span>
                    </div>
                    <div className="col-3">
                        <input
                            className="input input-sm center"
                            type="number"
                            value={quantity}
                            onChange={handleQuantityChange}
                            disabled={
                                !(
                                    currentCardActive ===
                                    CardActiveSequence.QUANTITY
                                )
                            }
                        />
                    </div>
                </div>
                <div className="col-12">
                    <CustomPrimaryButton
                        label="Done"
                        handleClick={handleQuantityButton}
                        isDisabled={
                            !(
                                quantity > 0 &&
                                currentCardActive ===
                                    CardActiveSequence.QUANTITY
                            )
                        }
                        borderRadius="0 0 4px 4px"
                    />
                </div>
            </Card>

            <Card className="mt-md">
                <div className="col-12 card p-md">
                    <h3 className="bold">Bad Stock</h3>
                    <p className="sm">Stock Damages or Expired</p>
                </div>
                {currentCardActive === 2 && (
                    <div className="grid m-center">
                        <div className="col-6">
                            <CustomPrimaryButton
                                label="Report Bad Stock"
                                handleClick={handleReportBadStockButton}
                                isDisabled={
                                    !(
                                        currentCardActive ===
                                        CardActiveSequence.BAD_STOCK
                                    )
                                }
                                secondary={true}
                                borderRadius="0 0 0 4px"
                            />
                        </div>
                        <div className="col-6">
                            <CustomPrimaryButton
                                label="Done"
                                handleClick={()=>setCurrentCardActive(3)}
                                isDisabled={
                                    !(
                                        currentCardActive ===
                                        CardActiveSequence.BAD_STOCK
                                    )
                                }
                                borderRadius="0 0 4px 0"
                            />
                        </div>
                    </div>
                )}
                {currentCardActive > 2 && (
                    <div className="grid m-center">
                        <div className="col-12">
                            <CustomPrimaryButton
                                label="Done"
                                handleClick={handleBadStockDone}
                                isDisabled={
                                    !(
                                        currentCardActive ===
                                        CardActiveSequence.BAD_STOCK
                                    )
                                }
                                borderRadius="0 0 4px 4px"
                            />
                        </div>
                    </div>
                )}
            </Card>

         

            {currentCardActive > 2 && (
                <div className="col-12 mt-md">
                    <CustomPrimaryButton
                        label="Submit"
                        handleClick={handleSubmitButton}
                        isDisabled={
                            !(
                                currentCardActive > 2
                            )
                        }
                    />
                </div>
            )}
        </Page>
    );
};
export const getServerSideProps: GetServerSideProps = async (context) => {
    const id = context.query.id;
    const itemid = context.query.itemid ? context.query.itemid : null;
    const storageid = context.query.storageid ? context.query.storageid : null;
    return {
        props: {
            id,
            itemid,
            storageid
        },
    };
};

export default AuditProductDetail;


