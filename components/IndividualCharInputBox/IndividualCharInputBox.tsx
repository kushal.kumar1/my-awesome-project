import React, { useEffect, useState } from "react";
import InputBox from "./InputBox";

interface Props {
    amount: number;
    autoFocus: any;
    handleOutputString: Function;
    inputRegExp: any;
    password?: boolean;
    type:string;
}

const IndividualCharInputBox = React.memo(function IndividualCharInputBox(
    props: Props
) {
    const [characterArray, setCharacterArray] = useState(
        Array(props.amount).fill(null)
    );

    const [inputSize] = useState(Array(props.amount).fill(""));

    //setting the focus to first element
    useEffect(() => {
        if (props.autoFocus) {
            const input: HTMLElement = document.querySelector("#input0");
            input.focus();
        }
    }, []);

    const handleChange = ({ target }) => {
        if (target.value.match(props.inputRegExp)) {
            focusNextChar(target);
            setModuleOutput(target);
        } else {
            target.value = characterArray[target.name.replace("input", "")];
        }
    };

    const handleKeyDown = ({ target, key }) => {
        if (key === "Backspace") {
            if (target.value === "" && target.previousElementSibling !== null) {
                target.previousElementSibling.value = "";
                focusPrevChar(target);
            } else {
                target.value = "";
            }
            setModuleOutput(target);
        } else if (key === "ArrowLeft") {
            focusPrevChar(target);
        } else if (key === "ArrowRight" || key === " ") {
            target;
        }
    };

    const handleFocus = ({ target }) => {
        var el = target;
        // In most browsers .select() does not work without the added timeout.
        setTimeout(function () {
            el.select();
        }, 0);
    };

    const focusPrevChar = (target) => {
        if (target.previousElementSibling !== null) {
            target.previousElementSibling.focus();
        }
    };

    const focusNextChar = (target) => {
        if (target.nextElementSibling !== null) {
            target.nextElementSibling.focus();
        }
    };

    const setModuleOutput = (target) => {
        const { name, value } = target;

        let tempCharacters = [...characterArray];
        //size of text input and after that will get the index value
        let index = name.slice("5");
        try {
            index = parseInt(index);
        } catch (error) {
            console.log(error);
        }

        tempCharacters[index] = value;
        setCharacterArray(tempCharacters);

        props.handleOutputString(tempCharacters.join(""));
    };

    return (
        <div>
            {inputSize.map((_, index) => (
                <InputBox
                    type={props.type}
                    key={index}
                    handleKeyDown={handleKeyDown}
                    handleFocus={handleFocus}
                    handleChange={handleChange}
                    name={"input" + index}
                    id={"input" + index}
                />
            ))}
        </div>
    );
});

export default IndividualCharInputBox;
