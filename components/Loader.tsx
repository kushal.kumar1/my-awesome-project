const Loader = () => {
    return (
        <div className="page-center">
            <img src="/images/robo.gif" />
        </div>
    );
};

export default Loader;