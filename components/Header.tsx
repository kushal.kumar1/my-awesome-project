import {useSelector} from "react-redux";
import {RootState, useThunkyDispatch} from "../redux/store";
import router, {useRouter} from "next/router";
import { useState } from "react";
import AppAction from "../redux/actions/AppAction";



const mapRootState = (rootState: RootState) => {
    return {
        warehouseName: rootState.appState.warehouse
    };
}




const Home = ({ openSidebar }) => {
    const {warehouseName} = useSelector(mapRootState);
    const [selectedwarehouse,setSelectedWarehouse] = useState(0);
    const [displayWareHouseList,setDisplayWareHouseList] = useState(false);
    const dispatch = useThunkyDispatch();
  //  const setWarehouse

  const logout =  () =>{
    console.log('logout')
     dispatch(AppAction.logout());  
}

const changeStore =() =>{
   setDisplayWareHouseList(!displayWareHouseList);
  //alert('change')
}
 
const changeWarehouse =(index) =>{
    console.log(warehouseName[index]);
    setSelectedWarehouse(index);
    dispatch(AppAction.switchStore(index));

}



const displayStores =() =>{
    return (<div style = {{zIndex:10,backgroundColor:'#262d33',position:'absolute', top:10,right:0}} className="col-3 right">
        
        {warehouseName && warehouseName.map((value,index)=>{
     //   console.log(value , index)
      let store  = null;
      let hidden = '';
       if (selectedwarehouse != index ) {
            if (!displayWareHouseList){
           hidden = 'hidden'
           }
       }
      store = <ul onClick={()=>changeWarehouse(index)}  style ={{margin:10,borderWidth:1}}className={`storename mr-xs`}>{warehouseName[index].name}</ul>;


      return  store;

   }) 
}

    </div>)

   
}

    return (
        <>
            <div className="col-3" onClick={() => logout()}>
                 <i className="fas fa-power-off"></i>
            </div>
            <div className="col-6 center">
                <div className="semi-bold">MERA 1K</div>
            </div>
            
            <div className="col-3 right" onClick={changeStore}>
                <div className="xs normal">
                    <span className="mr-xs">{warehouseName[selectedwarehouse] && warehouseName[selectedwarehouse].name}</span>
                    <span><i className="fas fa-map-marker-alt"></i></span>
                
                </div>{
                displayWareHouseList && displayStores()}
          
                  
            
            </div>
        </>
    );
}


const PageTitle = ({title}) => {
    const router = useRouter();
    return (
        <>
            <div className="col-3">
                <span onClick={() => router.back()}><i className="fas fa-angle-left"></i></span>
            </div>
            <div className="col-6 center">
                <div className="semi-bold">{title}</div>
            </div>
            <div className="col-3 right">
            </div>
        </>
    );
}

const Header = ({ title, openSidebar }) => {
    return (
        <header className="header">
            <div className="grid gutter-between c-center white p-md xs">
                {title ? <PageTitle title={title} /> : <Home openSidebar={openSidebar} />}
            </div>
        </header>
    );
}

export default Header;
