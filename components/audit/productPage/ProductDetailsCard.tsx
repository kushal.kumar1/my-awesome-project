import { NextPage } from "next";
import styled from "styled-components";
import { useState } from "react";

const EnlargedImage = styled.div`
    position: relative;
    display: flex;
    justify-content: center;

    .zoomIcon {
        position: absolute;
        top: -6px;
        right: -6px;
        cursor: pointer;
    }
`;

const ProductDetailsCard = ({ product }) => {
    const [isImageEnlarged, setIsImageEnlarged] = useState(false);

    const toggleImage = () => {
        setIsImageEnlarged(!isImageEnlarged);
    };

    return (
        <div className="grid gutter-md">
            {isImageEnlarged ? (
                <div className="col-12">
                    <EnlargedImage className="card">
                        <ProductImage productImage={product?.featuredImage} />
                        <span onClick={toggleImage}>
                            <img
                                src="/images/zoomIcon.png"
                                className="zoomIcon"
                            />
                        </span>
                    </EnlargedImage>
                </div>
            ) : (
                <>
                    <div className="col-5">
                        <EnlargedImage className="card">
                            <ProductImage productImage={product?.featuredImage} />
                            <span onClick={toggleImage}>
                                <img
                                    src="/images/zoomIcon.png"
                                    className="zoomIcon"
                                />
                            </span>
                        </EnlargedImage>
                    </div>
                    <div className="col-7">
                        <p className="sm">PID - {product.id}</p>
                        <h3 className="semi-bold">{product.name}</h3>
                        <h3 className="sm">
                            Barcode -{" "}
                            <span className="semi-bold">{product.barcode}</span>
                        </h3>
                    </div>
                </>
            )}
        </div>
    );
};

export default ProductDetailsCard;

const ProductImage = ({ productImage }) => {
    if (productImage) {
        return <img src={productImage} alt="product" />;
    }

    return (
        <img
            src="/images/robo.gif"
            alt="generic image as no product image found"
        />
    );
};
