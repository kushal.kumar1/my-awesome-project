import { useDispatch } from "react-redux";
import { ModalName } from "../ModalManager";
import AppAction from "../../redux/actions/AppAction";
import { AuditItemStatus } from "../../types/AuditItem";
import router from "next/router";

const AuditItem = ({ auditid, audittype,item, auditName }) => {
  const dispatch = useDispatch();
  console.log(audittype);
  const getQuery=(audittype, item) =>{
    if (audittype ==='product'){
        return 'product='+item.product.id+'&audit_name='+auditName
    } else {

      return 'location='+item.product.id+'&audit_name='+auditName+'&label='+item.product.label
    }

  }
  const goToDetailsPage = () => {
    const query = getQuery(audittype,item); 
   
    console.log('item --', item);
   // alert('message')
   router.push('/audit/'+auditid+'/items/?'+query);
  };
 

  return (
    <div className="col-12">
      <div className="card sm">
        <div className="flex">
          <div className="col-9 p-sm">
          <div className="bold sm">#{ audittype==='product'?item.id:item.product.id}</div>
            {audittype==='product'?
            <div className="bold sm">{item.product.name}</div>
            :
            <div className="bold sm">{item.product.label}</div>
            }<div className="flex">
              <div className="col-4 mt-xs s">Total Quantity: {item.totalQuantity}</div>
            </div>
            <div className="flex">
             { audittype =='product' ?
              <div className="col-8 mt-xs s">Total Locations: {item.numberOfStorages}</div>
              :
              <div className="col-8 mt-xs s">Total Products: {item.numberOfProducts}</div>
             }
            </div>
          </div>

          <div
            
            className={'col-3 pd-sm flex m-center c-center'}
          >
            
           
              <div onClick={goToDetailsPage}
              style={{ border: "none" }} className={`col-9 bg-black flex center mt-xs md m-center c-center`}>
                    {item.added_to_cart  ? "Completed" : "  View  "}
               <i
                  style={{ fontSize: "large" }}
                  className="fa fa-chevron-right center"
                ></i>
            
              </div>
          
          </div>
        </div>
      </div>
    </div>
  );
};

export default AuditItem;
