import { useDispatch } from "react-redux";
import { ModalName } from "../ModalManager";
import AppAction from "../../redux/actions/AppAction";
import { AuditItemStatus } from "../../types/AuditItem";
import { AppActionTypes } from "../../redux/ActionTypes";
import { useEffect, useState } from "react";

const AuditItem = ({ item,audittype, addProdcutToStore }) => {
  const dispatch = useDispatch();
  const [quantiy, setQuantity] = useState(item.systemQuantity);

   useEffect (()=>{

    if (item.systemQuantity == 0 && item.actualQuantity !== [] )  {
      console.log('actual quantity not empty ');
      let  quantity = 0;
      item.actualQuantity.forEach(item => {
        console.log('item.quantity', item.quantity);
        quantity =  quantity+item.quantity}
        )
        
      setQuantity(quantity);

    }

  },[])

  const triggerModal = () => {
    
        // Open scanner 
    if (item.status == AuditItemStatus.ASSIGNED) { 
        dispatch(
            AppAction.switchModal(ModalName.AUDIT, {
                item: item,
                audittype:audittype,
                addProdcutToStore:addProdcutToStore,
                storagelabel:item.storage ? item.storage.label : null

            })
        );
      }
      else {
        dispatch({
          type: AppActionTypes.DISPLAY_MESSAGE,
          payload: {text: "Item Audit  Cancelled or cannot be audited right now ", isError: true, milliseconds: 2000}
      });
      }
   
  };
 

  return (
    <div className="col-12">
      <div className="card sm">
        <div className="flex">
          <div className="col-9 p-sm">
          <div className="bold sm">#{item.id}</div>
            <div className="bold sm">{item.product.name}</div>
            <div className="flex">
              <div className="col-4 mt-xs s">Quantity: {quantiy}</div>
            </div>
            {audittype =='product' && 
            <div className="flex">

              <div className="col-8 mt-xs s">SL: {item.storage ? item.storage.label : "STORAGE NOT ASSSIGNED"}</div>
            
            </div>
            }
          </div>

          <div
            
            className={'col-3 pd-sm flex m-center c-center'}
          >
            
           
              <div onClick={triggerModal}
              style={{ border: "none" }} className={`col-9 ${item.status === AuditItemStatus.CANCELLED?'bg-red white':
                    item.status=== AuditItemStatus.PENDING_ACTION || item.status === AuditItemStatus.PENDING_APPROVAL?'bg-grey ':'bg-black'} flex center mt-xs md m-center c-center`}>
                    {item.status === AuditItemStatus.CANCELLED  ? "Cancelled" :
                     item.status===AuditItemStatus.PENDING_ACTION || item.status === AuditItemStatus.PENDING_APPROVAL ?"  Audit Done  ":"  View  "}
                   { item.status === AuditItemStatus.ASSIGNED&& <i
                  style={{ fontSize: "large" }}
                  className="fa fa-chevron-right center"
                ></i>
                   }
              
            
              </div>
          
          </div>
        </div>
      </div>
    </div>
  );
};

export default AuditItem;
