import moment from 'moment';
import Link from '../Link';
import Audit from '../../types/Audit';
import { AuditStatus } from '../../types/Audit';
import { useThunkyDispatch } from '../../redux/store';
import { AppActionTypes } from '../../redux/ActionTypes';
import router from 'next/router';

// change things to audit once APIs finalised
const AuditListItem = ({ item }: { item: any}) =>{

  const dispatch = useThunkyDispatch();

  const addDistinctProductToStore = async (item) =>{
    dispatch({
        type: AppActionTypes.ADD_DISTINCT_AUDIT_ITEM_PRODUCTS,
        payload: {distictAduitProducts: item.distinctProducts}
    });
    router.push(item.status ===AuditStatus.STARTED?
      item.type === `product`?`/audit/${item.id}/items?audit_name=${item.name}`:`/audit/${item.id}?audit_name=${item.name}`
      :'#')
  }

  return (
    <div className='mt-md' onClick={()=>addDistinctProductToStore(item)}>
      <div className="card p-md ripple">
        <div className="grid gutter-between no-select c-center">
          <div>
            <div className="grid m-center">
              
              <div className="col">
                <span className="semi-bold md">#{item.id}</span>
                <div>
                <span className="bold lg">{item.name}</span>
                </div>
                <div>
                <span className="semi-bold md blue">[ {item.type === 'product'? 'Item-wise':'Location-wise'} ]</span>
                </div>
                
              </div>
            </div>
          </div>
          <div>
            <span className={`tag-${item.status==AuditStatus.STARTED?'open': ''} 
            ${item.status === AuditStatus.CANCELLED ?'bg-red':
            item.status === AuditStatus.CANCELLED ?'bg-black':''}  tag upper`}>
              {item.status==AuditStatus.STARTED?'Open'
              :
              item.status ===AuditStatus.CANCELLED?'Cancelled'
              :
              item.status ===AuditStatus.COMPLETED?' Completed ':''}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AuditListItem;
