import Link from "../Link";

const DashCard = ({faIcon, label, color}) => {
    return (
        <Link href={"/" + label}>
            <div className="col-6">
                <div className="card ripple">
                    <div className="p-lg">
                        <i className={"fa-4x orange " + faIcon + " " + color}></i>
                    </div>
                    <hr className="hr light"/>
                    <div className="p-sm xs bold">
                        <span className="mr-sm upper">{label}</span>
                        <i className="fas fa-chevron-right"></i>
                    </div>
                </div>
            </div>
        </Link>
    );
}

export default DashCard;