import styled from "styled-components";



const Sidebar = ({ show, openSidebar, children }) => {
    const Root = styled.div`
        height: 100%;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: var(--color-blue-dark);
        opacity: 0.98;
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 60px;
        width: ${show ? "250px" : 0};

        a {
            padding: 8px 8px 8px 32px;
            text-decoration: none;
            font-size: 25px;
            color: var(--color-grey);
            display: block;
            transition: 0.3s;
        }

        a:hover {
            color: var(--color-white);
        }

        .closebtn {
            position: absolute;
            top: 0;
            right: 25px;
            font-size: 36px;
            margin-left: 50px;
        }
    `;

    return (
        <Root>
            <a href="javascript:void(0)" className="closebtn" onClick={() => openSidebar()}>×</a>
            {children}
        </Root>
    );
}

export default Sidebar;
