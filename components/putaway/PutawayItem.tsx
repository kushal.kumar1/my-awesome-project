import {useDispatch} from "react-redux";
import {ModalName} from "../../components/ModalManager";
import AppAction from "../../redux/actions/AppAction";

const PutawayItem = ({item, assignStorage, showStorageBtn}) => {

    const dispatch = useDispatch();

    const triggerModal = () => {
        if (showStorageBtn) {
            dispatch(AppAction.switchModal(ModalName.ASSIGN_STORAGE, {item: item, assignStorage: assignStorage}));
        }
    }

    return (
        <div className="col-12">
            <div className="card p-md sm">
                <div className="grid gutter-sm">
                    <div className="col-10">
                        <div className="semi-bold">{item.product.name}</div>
                        <div className="mt-xs xs">Quantity: {item.quantity}</div>
                        {
                            item.storage && <div className="xs">Storage: {item.storage.label}</div>
                        }
                    </div>
                    {
                        !item.storage && <div className="col-2">
                            <div style={{border: "none"}} className={`${showStorageBtn ? "orange" : "grey"} center mt-xs`}
                                 onClick={triggerModal}>
                                <i style={{fontSize: "x-large"}} className="fas fa-sign-in-alt"></i><br/>
                                <div className="xs">Assign<br/>Storage</div>
                            </div>
                        </div>
                    }
                </div>
            </div>
        </div>
    );
}

export default PutawayItem;













