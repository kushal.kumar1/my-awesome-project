import Modal from "../../../layouts/Modal";
import React, {FormEvent, useState} from 'react';
import AsyncSelect from 'react-select/async';
import PutawayApi from "../../../apis/PutawayApi";
import {useSelector} from "react-redux";
import {RootState} from "../../../redux/store";
import Toolkit from "../../../utils/Toolkit";

const AssignStorageModal = ({complete, item, assignStorage}) => {
    const [selectedStorage, setSelectedStorage] = useState(null);

    const handleChange = (value) => {
        setSelectedStorage(value.id);
    }

    const rootState = useSelector((state: RootState) => state);
    const apiProps = Toolkit.mapStateToApiProps(rootState);

    const loadStorages = async (value) => {
        const response = await new PutawayApi(apiProps).searchStorage(value);
        return response.data.data;
    };
    
    const submit = async (e: FormEvent) => {
        e.preventDefault();
        const params = {id: item.id, storage_id: selectedStorage};
        assignStorage(params);
        complete();
    }

    return (
        <Modal>
            <h1 className="title mt-lg">{item.product.name}</h1>
            <p>Quantity: {item.quantity}</p>
            <p>Barcode: {item.product.barcode}</p>
            <form className="mt-lg" onSubmit={submit}>
                <label className="label">Assign  to Storage</label>
                <AsyncSelect
                    getOptionLabel={e => e.label}
                    getOptionValue={e => e.id}
                    loadOptions={loadStorages}
                    onChange={handleChange}
                />
                <div className="mt-lg center">
                    <button className="btn btn-green">ASSIGN</button>
                </div>
            </form>
        </Modal>
    );
};

export default AssignStorageModal;
