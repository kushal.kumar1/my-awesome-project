import Modal from "../../../layouts/Modal";
import React, {FormEvent, useEffect, useState} from 'react';
import AsyncSelect from 'react-select/async';
import Toolkit from "../../../utils/Toolkit";
import {useSelector} from "react-redux";
import {RootState} from "../../../redux/store";
import PutawayApi from "../../../apis/PutawayApi";
import Select, { components }  from "react-select";
import {ItemSchema} from "../../../types/PutawayItem";
import {use} from "ast-types";

interface VariantOption {
    quantity: number,
    value: string,
    label: string
}

const NoOptionsMessage = props => {
    return (
        <components.NoOptionsMessage {...props}>
            <span>No Products Found</span>
        </components.NoOptionsMessage>
    );
};

const AddItemModal = ({complete, onSubmit, barcode}) => {
    const [selectedProduct, setSelectedProduct] = useState(null);
    const [quantity, setQuantity] = useState(null);
    const [variantType, setVariantType] = useState<VariantOption | null>(null);
    const [variants, setVariants] = useState<VariantOption[]>([]);
    const [totalUnits, setTotalUnits] = useState(0);
    const [errors, setErrors] = useState<{ [prop: string]: string[] }>({});

    const selectProduct = (value) => {
        const variants: VariantOption[] = value.variants.map((v: any) => {
            return {value: v.type, label: v.type, quantity: v.quantity}
        });
        setSelectedProduct(value.id);
        setVariants(variants);
    }

    const rootState = useSelector((state: RootState) => state);
    const apiProps = Toolkit.mapStateToApiProps(rootState);

    const loadProducts = async (value) =>{
        const val = value || barcode;
        const response = await new PutawayApi(apiProps).searchProduct(val);
        return response.data.data;
    };

    const onVariantChange = (value) => {
        setVariantType(value);
    };

    const setQty = (e) => {
        setQuantity(e.target.value);
        const total = e.target.value * variantType.quantity;
        setTotalUnits(total);
    }

    const submit = async (e: FormEvent) => {
        e.preventDefault();
        const data = {
            product: selectedProduct,
            sku: variantType,
            quantity: totalUnits
        }

        const errors = Toolkit.validate(ItemSchema, data);
        setErrors(errors);
        if (Object.keys(errors).length == 0) {
            onSubmit(data);
            complete();
        }
    }

    return (
        <Modal>
            <h1 className="title mt-lg">Add Item</h1>
            <form className="mt-lg" onSubmit={submit}>
                <label className="label">Product</label>
                <AsyncSelect
                    components={{ NoOptionsMessage }}
                    getOptionLabel={e => `${e.name}: ${e.barcode}`}
                    getOptionValue={e => e.id}
                    loadOptions={loadProducts}
                    onChange={selectProduct}
                    defaultInputValue={barcode}
                    defaultOptions={!!barcode}
                    defaultMenuIsOpen={!!barcode}
                />
                <span className="error">{errors.product ? errors.product[0] : null}</span>
                <div className="mt-md">
                    <label className="label">Variant type</label>
                    <Select
                        options={variants}
                        onChange={(val) => onVariantChange(val)}
                    />
                    <span className="error">{errors.sku ? errors.sku[0] : null}</span>
                </div>
                <div className="mt-md">
                    <label className="label">Quantity</label>
                    <input onChange={(e) => setQty(e)} className="input input-sm" type="number"/>
                    <span className="error">{errors.quantity ? errors.quantity[0] : null}</span>
                </div>

                <div className="mt-md">
                    <label className="label">Total Units: </label>
                    <div>{totalUnits}</div>
                </div>

                <div className="mt-lg right">
                    <button className="btn btn-green">ADD</button>
                </div>
            </form>
        </Modal>
    );
};

export default AddItemModal;
