import { useDispatch } from "react-redux";
import { ModalName } from "../ModalManager";
import AppAction from "../../redux/actions/AppAction";
import { TransferItemStatus } from "../../types/TransferItem";

const TransferItem = ({ item, showStorageBtn, addToCart, migrationId, remarks }) => {
  const dispatch = useDispatch();

  const triggerModal = () => {
    if (showStorageBtn) {
      dispatch(
        AppAction.switchModal(ModalName.TRANSFER, {
          item: item,
          addToCart: addToCart,
          migrationId: migrationId,
          remarks: remarks
        })
      );
    }
  };

  return (
    <div className="col-12">
      <div className="card sm">
        <div className="flex">
          <div className="col-9 p-sm">
            <div className="semi-bold sm">{item.product.name}</div>
            <div className="flex">
              <div className="col-8 mt-xs xs">Barcode: {item.product.barcode}</div>
              <div className="col-4 mt-xs xs">
                Qty: {item.quantity} 
                {console.log("quantity$$$", item.placed_quantity != 0)} 
                ({item.placed_quantity && 
                item.placed_quantity})
                </div>
            </div>
            <div className="flex">
              <div className="col-8 mt-xs xs">SL: {item.source_storage.label}</div>
              <div className="col-4 mt-xs xs">DL: {item.destination_storage && item.destination_storage.label}</div>
            </div>
          </div>

          <div
            onClick={item.cart_quantity == 0 ? triggerModal: undefined}
            className={`${
              showStorageBtn
                ? item.cart_quantity > 0 ? "bg-green" : "bg-orange"
                : "bg-grey-light"
            }  col-3 pd-sm flex m-center c-center`}
          >
              <div style={{ border: "none" }} className={`center mt-xs`}>
                <i
                  style={{ fontSize: "x-large" }}
                  className={`fas ${item.cart_quantity > 0 ? 
                    item.status == "completed" ? "fa-clipboard-check green" : "fa-cart-plus"
                    : 
                    "fa-cart-arrow-down"} `}
                ></i>
                <br />
                {item.status == 'completed'? 
                <div className="xs green">Completed</div>
                : 
                <div className="xs">{item.cart_quantity > 0  ? "Added" : "Add"}</div>
              }
              </div>
          
          </div>
        </div>
      </div>
    </div>
  );
};

export default TransferItem;
