import { useDispatch } from "react-redux";
import { ModalName } from "../ModalManager";
import AppAction from "../../redux/actions/AppAction";
import { TransferItemStatus } from "../../types/TransferItem";

const CartItem = ({ migrationItems, item , migrationstatus , putawayItem, migrationId}) => {
    const dispatch = useDispatch();
     const label = item.placed_quantity == null ? 'MIGRATE' : 'MIGRATED';
    const triggerModal = () => {
        dispatch(
            AppAction.switchModal(ModalName.MIGRATION, {
                item: item,
                migrationItems: migrationItems,
                putawayItem: putawayItem,
                migrationId
            })
        );
    }

    return (
        <div className="col-12">
            <div className="card sm">
                <div  className="flex">
                    <div className="col-9 p-sm">
                        {/* <div className="semi-bold">{item.id}</div> */}
                        <div className="semi-bold sm">{item.product.name}</div>
                        {/* <div className="mt-xs xs">Quantity: {item.quantity}</div> */}
                        <div className="flex">
                            <div className="col-8 mt-xs xs">Barcode: {item.product.barcode}</div>
                            <div className="col-4 mt-xs xs">Qty: {item.quantity} {item.placed_quantity && `(${item.placed_quantity})`}</div>
                        </div>
                        <div className="flex">
                            <div className="col-4 mt-xs xs">DL: {item.storage && item.storage.label}</div>
                        </div>
                    </div>
                    
                    <div
                       style ={{borderWidth:1 ,borderTopRightRadius:10,
                        borderBottomRightRadius: item.storage && item.storage.type==="excess" ?0: 10}}
                        className={`${item.placed_quantity == null ?
                            (migrationstatus && item.placed_quantity == null ) ? "bg-orange": "bg-grey" :
                            "bg-green" }
                        col-3 pd-sm flex m-center c-center`}
                    >
                        <div
                            style={{ border: "none" }}
                            className={` center mt-xs`}
                            onClick={(migrationstatus && item.placed_quantity == null )?  triggerModal: undefined}
                        >
                            <i
                                style={{ fontSize: "x-large" }}
                                className="fas fa-random"
                            ></i>
                            <br />
                            <div className="xs">{label}</div>
                            
                        </div>
                    </div>
                    { item.storage && item.storage.type==="excess" && <>
                     <div className="col-9 p-sm">
                    </div> 
                    <div style = {{borderBottomRightRadius:10 }} className="col-3 pd-sm flex bg-blue m-center c-center">Excess</div>
                    </>   }    
                </div>
            </div>
        </div>
    );
};

export default CartItem;
