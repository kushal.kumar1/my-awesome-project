import Product from "./Product";
import * as yup from 'yup';

export interface PutawayItem {
    id: number,
    product: Product,
    quantity: number,
    storageId?: number
}

export const ItemSchema = yup.object().shape({
    product: yup.number().required(),
    sku: yup.object().required(),
    quantity: yup.string().required()
});
