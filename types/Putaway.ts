import {PutawayItem} from "./PutawayItem";

export enum PutawayStatus {
    OPEN = "open",
    STARTED = "started",
    CLOSED = "closed",
}

export default interface Putaway {
    id: number,
    status: PutawayStatus,
    created_at: number,
    items: PutawayItem[]
}
