import {AuditItem} from "./AuditItem";
import { LocationItem } from "./LocationItem";
import Product from "./Product";

export enum AuditStatus {
    
    STARTED = "started",
    COMPLETED = "completed",
    CANCELLED = "cancelled",
}

export interface location {
    id : number,
    type : string,
    warehouse : {}
}
export interface storage {
    product: location,
    category: string,
    label: string

}
export interface productGroup{
    product:Product,
    totalQuantity:number,
    numberOfStorages:number
}
export interface storageGroup{
    product:storage,
    totalQuantity:number,
    numberOfProducts:number
}

export default interface Audit {
    id: number,
    name: string,
    type : string,
    status: AuditStatus,
    distinctProducts: productGroup[], // optional 
    distinctStorages:storageGroup[]

}


    