# Name the node stage "builder"
FROM node:14
# Set working directory
RUN mkdir -p /usr/src
WORKDIR /usr/src

# Copy all files from current directory to working dir in image
COPY . /usr/src
# install node modules and build assets
RUN npm install

# start app
RUN npm run build
#CMD npm run dev
EXPOSE 4000
CMD ["npm", "run" , "dev"]