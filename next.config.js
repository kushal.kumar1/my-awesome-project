const withPWA = require("next-pwa");
const nextConfig = {
    useFileSystemPublicRoutes: true,
    // webpack5: false,
    pwa: {
        dest: 'public',
        disable: process.env.NODE_ENV === 'development',
        // dynamicStartUrlRedirect: '/login'
    }
};

const dev = process.env.NODE_ENV !== 'production';
module.exports = dev ? nextConfig : withPWA(nextConfig);