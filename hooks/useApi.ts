import {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {RootState} from "../redux/store";
import Toolkit from "../utils/Toolkit";
import Api from "../apis/Api";
import {auditlist} from "../apis/dummydata";


interface ApiConfig {
    path: string,
    params?: any,
}

const useApi = <T>(config: ApiConfig) => {
    const [results, setResults] = useState<T | null>(null);
    const [loading, setLoading] = useState(false);
    const rootState = useSelector((state: RootState) => state);
    const apiProps = Toolkit.mapStateToApiProps(rootState);

    const callApi = async (config) => {
        setLoading(true);
        
        const apiClient = new Api(apiProps).getClient();
        const options = {
            url: config.path,
            baseURL: process.env.NEXT_PUBLIC_API_ENDPOINT,
            params: config.params,
        };
        //TODO REMOVE THIS coomment  IF ELSE ONCE API IS complete
       /**  if (config.path =='audits'){
            setResults(auditlist);
            setLoading(false);
        } else {*/
        //const response = await apiClient.request(options);
       // setResults(response.data.data);
        //setLoading(false);
    

    const response = await apiClient.request(options);
    console.log("config", config);
    console.log('cart response ', response.data)
    setResults(response.data.data);
    setLoading(false);
      //  }
}

    useEffect(() => {
        if(config.path) {
            callApi(config);
        }
    }, [config.path]);

    return {results, loading};
}

export default useApi;