import {ThunkyAction} from "../store";
import TransferAPI from "../../apis/TransferAPI";
import Toolkit from "../../utils/Toolkit";
import {AppActionTypes} from "../ActionTypes";
import {} from "../../types/PutawayItem";
import { TransferItem } from "../../types/TransferItem";


const addItem = (id, productId: number, quantity: number, storage_location:number, remarks:string, migration_item_id: number): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiParams = {
            product_id: productId,
            source_storage_id:storage_location,
            quantity: quantity,
            remarks: remarks,
            migration_item_id: migration_item_id
        };
        const apiProps = Toolkit.mapStateToApiProps(getState());
        try {
            const response = await new TransferAPI(apiProps).addItemToCart(apiParams);
            if(response.data.error){
                throw response.data.error
            }
            return response.data.data;
        }
        catch(e) {
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: e.response ? e.response.data.message : e, isError: true, milliseconds: 2000}
            });
        }

    }
}

//TO DO changes
const load = (id): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
        const response = await new TransferAPI(apiProps).loadItems(id);
        //return response;
        console.log("data   ", response);
        dispatch({
            type: AppActionTypes.LOAD_TRANSFERLIST,
            payload: {transferlist: response.data.data}
        });
        return response.data.data;
    }
}

const loadCart = (pid): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
        const response = await new TransferAPI(apiProps).loadCart(pid);
        //return response;
        return response.data.data;
    }
}

const startMigration = (id): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
        const response = await new TransferAPI(apiProps).startMigration(id);
        dispatch({
            type: AppActionTypes.START_MIGRATION,
            payload: {putawaystatus: 'started'}
        });
        return response.data;
    }
}

const putawayItem = (id , apiParams): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
       try{
        const response = await new TransferAPI(apiProps).migrateItem(id, apiParams);
        if(response.data.error){
            throw response.data.error
        }
        //return response;
        console.log("data   ", response);
        dispatch({
            type: AppActionTypes.MIGRATE_ITEM_SUCCESS,
            payload: {putawayItemMessage: response.data}
        });
        return response.data;
    } catch (err) {
            console.log('errorr',err)
        dispatch({
            type: AppActionTypes.DISPLAY_MESSAGE,
            payload: {text: err.response ? err.response.data.message : err, isError: true, milliseconds: 2000}
        });

    }
    }
}

const closeMigration = (id): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
        const response = await new TransferAPI(apiProps).closeMigration(id);
        dispatch({
            type: AppActionTypes.START_MIGRATION,
            payload: {putawaystatus: 'closed'}
        });
        return response.data;
    }
}
export default {
     addItem, load, startMigration,closeMigration, putawayItem, loadCart
}

