import {AppActionTypes} from "../ActionTypes";
import AppApi from "../../apis/AppApi";
import Toolkit from "../../utils/Toolkit";
import {ModalName} from "../../components/ModalManager";
import Storage, {StorageKey} from "../../utils/Storage";
import {ThunkyAction} from "../store";

const initialize = (): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const token = Storage.get(StorageKey.TOKEN, null);
        const user = Storage.get(StorageKey.USER, null);
        const warehouse = Storage.get(StorageKey.WAREHOUSE, null);
        const activeWarehouseIndex = Storage.get(StorageKey.ACTIVE_WAREHOSUE, 0);
        const payload = { token, user, warehouse, activeWarehouseIndex };
        dispatch({ type: AppActionTypes.INITIALIZE, payload });
    }
}

const displayMessage = (text: string, isError: boolean, milliseconds: number = 3000) => {
    return { type: AppActionTypes.DISPLAY_MESSAGE, payload: { text, isError, milliseconds } };
}

const switchModal = (name: ModalName, data: any = {}) => {
    return { type: AppActionTypes.MODAL_SWITCH, payload: { name, data } };
}

const sendOtp = ({ mobile }): ThunkyAction => {
    mobile = "+91" + mobile;
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
        await new AppApi(apiProps).sendOtp({ mobile })
            .then(response => {
                dispatch({ type: AppActionTypes.DISPLAY_MESSAGE, payload: { text: "OTP Sent successfully.", isError: false } });
            })
            .catch(error => {
                const {data} = error.response;
                let text = "UUnable to send OTP. Please enter correct mobile."
                
                if (data && data.message) {
                    text = data.message;
                }

                dispatch({ type: AppActionTypes.DISPLAY_MESSAGE, payload: { text, isError: true } });
            });
    };
}

const login = ({ mobile, otp }): ThunkyAction => {
    mobile = "+91" + mobile;
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
        await new AppApi(apiProps).login({ mobile, otp })
            .then(response => {
                const result = response.data;
                Storage.set(StorageKey.TOKEN, result.token);
                Storage.set(StorageKey.USER, result.data);
                Storage.set(StorageKey.WAREHOUSE, result.data.warehouse);
                Storage.set(StorageKey.ACTIVE_WAREHOSUE, 0);
                dispatch({ type: AppActionTypes.LOGIN_SUCCESS, payload: result });
            })
            .catch(error => {
                const {data} = error.response;
                let text = "Unable to login. Please enter correct credentials."
                
                if (data && data.message) {
                    text = data.message;
                }

                dispatch({ type: AppActionTypes.DISPLAY_MESSAGE, payload: { text, isError: true } });
            });
    };
}


const logout = (): ThunkyAction => {
    return async (dispatch, getState) => {
          dispatch({ type: AppActionTypes.LOGOUT_SUCCESS, payload: '' });
          Storage.clear();
          location.reload(); 
    };
}

const switchStore = (storeid:number): ThunkyAction => {
     let payload ={index:storeid}
    return async (dispatch, getState) => {
        const activeWarehouseIndex = Storage.set(StorageKey.ACTIVE_WAREHOSUE, storeid);
          dispatch({ type: AppActionTypes.WAREHOUSE_SWITCH, payload: payload });
          //location.reload(); 
    };
}

export default {
    initialize, displayMessage,
    switchModal, sendOtp, login, logout, switchStore
};
