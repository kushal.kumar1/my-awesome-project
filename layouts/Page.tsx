import React, {useEffect, useState} from 'react';
import Head from 'next/head';
import Header from '../components/Header';
import {useDispatch, useSelector} from 'react-redux';
import AppActions from '../redux/actions/AppAction';
import ModalManager, {ModalName} from '../components/ModalManager';
import Sidebar from '../components/Sidebar';
import {RootState} from "../redux/store";
import AppAction from "../redux/actions/AppAction";

interface Props {
    title?: any,
    children: any
}

const mapRootState = (rootState: RootState) => {
    return {
        message: rootState.appState.message,
    };
}

const Page = ({ title, children }: Props) => {

    const [showSideBar, setShowSidebar] = useState(false);
    const {message} = useSelector(mapRootState);
    const dispatch = useDispatch();
    // const logout = () => dispatch(AppActions.logout());
    //
    // const logoutConfirm = () => {
    //     dispatch(AppActions.switchModal(ModalName.CONFIRM, { action: logout, message: "Are you sure you want to logout?"}));
    // }

    useEffect(() => {
        if (message) {
            const timeoutId = setTimeout(() => dispatch(AppAction.displayMessage(null, false)), message.milliseconds);
            return () => clearTimeout(timeoutId);
        }
    }, [message]);

    return (
        <React.Fragment>
            <Head>
                <title>Mera 1K</title>
                <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0, width=device-width" />
                <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet" />
                <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
                <link rel="icon" href="/favicon.ico" type="image/x-icon" />
                <link rel="manifest" href="/manifest.json" />
            </Head>
            <ModalManager />

            {/*<Sidebar openSidebar={() => setShowSidebar(!showSideBar)} show={showSideBar} >*/}
            {/*    <a href="#">Link 1</a>*/}
            {/*    <a href="#">Link 2</a>*/}
            {/*    <a href="#">Link 3</a>*/}
            {/*    <a href="#">Link 4</a>*/}
            {/*</Sidebar>*/}
            <Header openSidebar={() => setShowSidebar(!showSideBar)} title={title} />
            {message && <div className="toast">{message.text}</div>}
            <div className="content">
                <div className="container">{children}</div>
            </div>
        </React.Fragment>
    );
};

export default Page;